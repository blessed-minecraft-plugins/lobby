package fr.blessedraccoon.minecraft.plugins.lobby;

import fr.blessedraccoon.minecraft.plugins.inventorygui.FixedItems;
import fr.blessedraccoon.minecraft.plugins.lobby.compass.HotbarGames;
import fr.blessedraccoon.minecraft.plugins.lobby.shop.HotbarShop;
import fr.blessedraccoon.minecraft.plugins.serverapi.server.Server;
import fr.blessedraccoon.minecraft.plugins.serverapi.server.ServerSwitcher;
import me.vagdedes.mysql.database.SQL;
import net.kyori.adventure.text.Component;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Lobby extends JavaPlugin {

    public void games(FixedItems fixedItems) {
        ItemStack games = new ItemStack(Material.COMPASS);
        var gamesMeta = games.getItemMeta();
        gamesMeta.displayName(Component.text(ChatColor.BLUE + "Jeux").append(Component.text(ChatColor.GRAY + " (Clic Droit)")));
        games.setItemMeta(gamesMeta);
        HotbarGames hg = new HotbarGames(games, 0, fixedItems, this);
    }

    public void shop(FixedItems fixedItems) {
        ItemStack shop = new ItemStack(Material.DIAMOND);
        var shopMeta = shop.getItemMeta();
        shopMeta.displayName(Component.text(ChatColor.GOLD + "Magasin").append(Component.text(ChatColor.GRAY + " (Clic Droit)")));
        shop.setItemMeta(shopMeta);
        HotbarShop hs = new HotbarShop(shop, 4, fixedItems);
    }

    @Override
    public void onEnable() {

        FixedItems fixedItems = new FixedItems(this);
        fixedItems.activate();
        this.games(fixedItems);
        this.shop(fixedItems);
        SQL.createTable("BallsOfSteelPerks", "UUID VARCHAR(36), PerkID INT(11), Level INT(11)");
    }
}
