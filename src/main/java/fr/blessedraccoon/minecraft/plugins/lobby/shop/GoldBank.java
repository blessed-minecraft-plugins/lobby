package fr.blessedraccoon.minecraft.plugins.lobby.shop;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import de.NeonnBukkit.CoinsAPI.API.CoinsAPI;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GoldBank {

    private OutlinePane pane;

    public GoldBank(OutlinePane pane) {
        this.pane = pane;
    }

    public void link(Player player) {
        this.link(player.getUniqueId().toString());
    }

    public void link(String uuid) {
        ItemStack item = new ItemStack(Material.GOLD_NUGGET);
        ItemMeta itemMeta = item.getItemMeta();
        int goldAmount = CoinsAPI.getCoins(uuid);
        String gold = goldAmount + " Coins";
        itemMeta.displayName(Component.text(gold).color(TextColor.color(200, 190, 0)));
        item.setItemMeta(itemMeta);

        pane.addItem(new GuiItem(item, event -> {}));
    }


}
