package fr.blessedraccoon.minecraft.plugins.lobby;

import fr.blessedraccoon.minecraft.plugins.inventorygui.FixedItems;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.Objects;

public abstract class HotbarItem {

    protected ItemStack item;
    protected int hotbarPosition;

    public HotbarItem(ItemStack item, int hotbarPosition, FixedItems fixedItems) {
        this.item = item;
        this.hotbarPosition = hotbarPosition;
        fixedItems.addItem(item);
    }

    @EventHandler
    public void treatClick(PlayerInteractEvent e) {
        if (Objects.equals(e.getItem(), item)) {
            e.setCancelled(true);
            this.onClick(e);
        }
    }

    public abstract void onClick(PlayerInteractEvent e);
}
