package fr.blessedraccoon.minecraft.plugins.lobby.shop.ballsofsteel;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import fr.blessedraccoon.minecraft.plugins.lobby.shop.GoldBank;
import fr.blessedraccoon.minecraft.plugins.lobby.shop.ballsofsteel.perks.PerksManager;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.function.Consumer;

public class PerkOneUseChoose implements Consumer<InventoryClickEvent> {
    @Override
    public void accept(InventoryClickEvent e) {
        ChestGui gui = new ChestGui(6, "Magasin - Balls of Steel");

        gui.setOnGlobalClick(event -> event.setCancelled(true));

        OutlinePane background = new OutlinePane(0, 0, 9, 6, Pane.Priority.LOWEST);
        background.addItem(new GuiItem(new ItemStack(Material.BLACK_STAINED_GLASS_PANE)));
        background.setRepeat(true);

        gui.addPane(background);

        OutlinePane navigationPane = new OutlinePane(3, 2, 1, 1);
        this.nonPermanent(navigationPane);

        // --------------------------------

        OutlinePane navigationPane2 = new OutlinePane(5, 2, 1, 1);
        this.perks(navigationPane2);

        // --------------------------------

        OutlinePane navigationPane3 = new OutlinePane(4, 4, 1, 1);
        new GoldBank(navigationPane3).link(e.getWhoClicked().getUniqueId().toString());

        gui.addPane(navigationPane);
        gui.addPane(navigationPane2);
        gui.addPane(navigationPane3);

        gui.show(e.getWhoClicked());
    }

    private void nonPermanent(OutlinePane pane) {
        ItemStack shop = new ItemStack(Material.WEATHERED_COPPER);
        ItemMeta shopMeta = shop.getItemMeta();
        shopMeta.displayName(Component.text("Bonus utilisables une fois").color(TextColor.color(200, 100, 0)));
        shopMeta.lore(List.of(Component.text("Bientôt disponible...").color(TextColor.color(Color.RED.asRGB()))));
        shop.setItemMeta(shopMeta);

        pane.addItem(new GuiItem(shop, event -> {}));
    }

    private void perks(OutlinePane pane) {
        ItemStack shop = new ItemStack(Material.DIAMOND_PICKAXE);
        ItemMeta shopMeta = shop.getItemMeta();
        shopMeta.displayName(Component.text("Perks Permanents").color(TextColor.color(200, 100, 0)));
        shop.setItemMeta(shopMeta);

        pane.addItem(new GuiItem(shop, event -> new PerksManager().accept(event)));
    }
}
