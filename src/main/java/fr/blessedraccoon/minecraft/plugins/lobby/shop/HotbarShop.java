package fr.blessedraccoon.minecraft.plugins.lobby.shop;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import fr.blessedraccoon.minecraft.plugins.inventorygui.FixedItems;
import fr.blessedraccoon.minecraft.plugins.lobby.HotbarItem;
import fr.blessedraccoon.minecraft.plugins.lobby.shop.ballsofsteel.PerkOneUseChoose;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class HotbarShop extends HotbarItem {

    public HotbarShop(ItemStack games, int i, FixedItems fixedItems) {
        super(games, i, fixedItems);
    }

    @Override
    public void onClick(PlayerInteractEvent e) {
        ChestGui gui = new ChestGui(6, "Magasin");

        gui.setOnGlobalClick(event -> event.setCancelled(true));

        OutlinePane background = new OutlinePane(0, 0, 9, 6, Pane.Priority.LOWEST);
        background.addItem(new GuiItem(new ItemStack(Material.BLACK_STAINED_GLASS_PANE)));
        background.setRepeat(true);

        gui.addPane(background);

        OutlinePane navigationPane = new OutlinePane(4, 2, 1, 1);
        ItemStack shop = new ItemStack(Material.HEART_OF_THE_SEA);
        ItemMeta shopMeta = shop.getItemMeta();
        shopMeta.displayName(Component.text("Balls of Steel").color(TextColor.color(200, 100, 0)));
        shop.setItemMeta(shopMeta);

        navigationPane.addItem(new GuiItem(shop, event -> new PerkOneUseChoose().accept(event)));

        OutlinePane navigationPane2 = new OutlinePane(4, 4, 1, 1);
        new GoldBank(navigationPane2).link(e.getPlayer());

        gui.addPane(navigationPane);
        gui.addPane(navigationPane2);

        gui.show(e.getPlayer());
    }
}
