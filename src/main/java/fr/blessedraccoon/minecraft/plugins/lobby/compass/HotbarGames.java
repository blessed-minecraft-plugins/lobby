package fr.blessedraccoon.minecraft.plugins.lobby.compass;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import fr.blessedraccoon.minecraft.plugins.inventorygui.FixedItems;
import fr.blessedraccoon.minecraft.plugins.lobby.HotbarItem;
import fr.blessedraccoon.minecraft.plugins.serverapi.server.Server;
import fr.blessedraccoon.minecraft.plugins.serverapi.server.ServerSwitcher;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

public class HotbarGames extends HotbarItem {

    private ServerSwitcher ballsOfSteel;

    public HotbarGames(ItemStack games, int i, FixedItems fixedItems, Plugin plugin) {
        super(games, i, fixedItems);
        this.ballsOfSteel = new ServerSwitcher(Server.BALLS_OF_STEEL_1, plugin);
    }

    @Override
    public void onClick(PlayerInteractEvent e) {
        ChestGui gui = new ChestGui(5, "Sélectionner un jeu");

        gui.setOnGlobalClick(event -> event.setCancelled(true));

        OutlinePane background = new OutlinePane(0, 0, 9, 5, Pane.Priority.LOWEST);
        background.addItem(new GuiItem(new ItemStack(Material.BLACK_STAINED_GLASS_PANE)));
        background.setRepeat(true);

        gui.addPane(background);

        OutlinePane navigationPane = new OutlinePane(4, 2, 3, 1);

        ItemStack shop = new ItemStack(Material.HEART_OF_THE_SEA);
        ItemMeta shopMeta = shop.getItemMeta();
        shopMeta.displayName(Component.text("Balls of Steel").color(TextColor.color(200, 100, 0)));
        shop.setItemMeta(shopMeta);

        navigationPane.addItem(new GuiItem(shop, event -> {
            this.ballsOfSteel.move(e.getPlayer());
        }));

        gui.addPane(navigationPane);

        gui.show(e.getPlayer());
    }

}
