package fr.blessedraccoon.minecraft.plugins.lobby.shop.ballsofsteel.perks;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.PaginatedPane;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import fr.blessedraccoon.minecraft.plugins.lobby.shop.GoldBank;
import fr.blessedraccoon.minecraft.plugins.shopperks.shop.*;
import me.vagdedes.mysql.database.MySQL;
import net.kyori.adventure.text.Component;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

public class PerksManager implements Consumer<InventoryClickEvent> {
    @Override
    public void accept(InventoryClickEvent e) {

        ChestGui gui = new ChestGui(6, "Perks");
        gui.setOnGlobalClick(event -> event.setCancelled(true));

        PaginatedPane pane = new PaginatedPane(0, 0, 9, 5);
        this.pagination(pane, (Player) e.getWhoClicked());

        gui.addPane(pane);
        this.pagination(gui, pane);

        gui.show(e.getWhoClicked());
    }

    public void pagination(PaginatedPane pane, Player player) {
        List<ItemPerk> perks = Perks.ballsOfSteelItemPerks;

        var uuid = player.getUniqueId().toString();
        Map<Integer, Integer> map = null;
        try {
            map = this.getBOSPerks(uuid);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(int i = 0 ; i < perks.size() ; i++) {

            final int pageIndex = i / 5;
            Perk perk = perks.get(i);
            int nbLevels = perk.getPerksLevels().size();
            StaticPane page = new StaticPane(0, i % 5, nbLevels + 1, 1);
            page.addItem(new GuiItem(perk.getItem(), event -> {}), 0, i % 5);

            for(int j = 0 ; j < perk.getPerksLevels().size() ; j++) {

                var levelCost = perk.getPerksLevels().get(j);
                var item = this.getGlass(map, perk.getPerkID(), levelCost, player);
                page.addItem(new GuiItem(item, event -> {
                    if (item.getType() != Material.GREEN_STAINED_GLASS_PANE) {
                        perk.accept(event);
                    }
                }), j + 1, i % 5);

            }
            if (i % 5 == 4 || i == perks.size() - 1) {
                pane.addPane(pageIndex, page);
            }
            if (i % 5 == 0) {
                OutlinePane gold = new OutlinePane(4, 5, 1, 1);
                new GoldBank(gold).link(player);
            }

        }
    }

    private Map<Integer, Integer> getBOSPerks(String uuid) throws SQLException {
        var map = new HashMap<Integer, Integer>();
        var result = MySQL.query("SELECT perkID, level FROM BallsOfSteelPerks WHERE UUID = '" + uuid + "'");
        while (result.next()) {
            int perkID = result.getInt(1);
            int level = result.getInt(2);
            map.put(perkID, level);
        }
        return map;
    }

    private ItemStack getGlass(Map<Integer, Integer> map, int ID, LevelCostDescription lcp, Player player) {

        int level = Objects.requireNonNullElse(map.get(ID), 0);

        var item = new ItemStack(Material.RED_STAINED_GLASS_PANE);
        var meta = item.getItemMeta();
        meta.displayName(lcp.getName());
        final int cost = lcp.getCost();

        if (level + 1 < lcp.getLevel() || !Coins.canAfford(player, cost)) {
            meta.lore(List.of(
                    lcp.getDescription(),
                    Component.text(ChatColor.GOLD.toString() + cost),
                    Component.text(ChatColor.RED + "Ne peut pas être acheté")));
            item.setItemMeta(meta);
        }
        else if (level + 1 == lcp.getLevel() && Coins.canAfford(player, cost)) {
            item.setType(Material.YELLOW_STAINED_GLASS_PANE);
            meta.lore(List.of(
                    lcp.getDescription(),
                    Component.text(ChatColor.GOLD.toString() + cost),
                    Component.text(ChatColor.YELLOW + "Peut être acheté")));
            item.setItemMeta(meta);
        } else {
            item.setType(Material.GREEN_STAINED_GLASS_PANE);
            meta.lore(List.of(
                    lcp.getDescription(),
                    Component.text(ChatColor.GOLD.toString() + cost),
                    Component.text(ChatColor.GREEN + "Déjà acheté")));
            item.setItemMeta(meta);
        }
        return item;
    }

    public void pagination(ChestGui gui, PaginatedPane pane) {
        //page selection
        StaticPane back = new StaticPane(0, 5, 1, 1);
        StaticPane forward = new StaticPane(8, 5, 1, 1);

        back.addItem(new GuiItem(new ItemStack(Material.ARROW), event -> {
            pane.setPage(pane.getPage() - 1);

            if (pane.getPage() == 0) {
                back.setVisible(false);
            }

            forward.setVisible(true);
            gui.update();
        }), 0, 0);

        back.setVisible(false);
        if (pane.getPages() == 1) {
            forward.setVisible(false);
        }

        forward.addItem(new GuiItem(new ItemStack(Material.ARROW), event -> {
            pane.setPage(pane.getPage() + 1);

            if (pane.getPage() == pane.getPages() - 1) {
                forward.setVisible(false);
            }

            back.setVisible(true);
            gui.update();
        }), 0, 0);

        gui.addPane(back);
        gui.addPane(forward);
    }
}
